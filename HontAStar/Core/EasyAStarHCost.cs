﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hont.AStar
{
    public class EasyAStarHCost : IAStarHCost
    {
        public int GetCost(Position currentPos, Position targetPos)
        {
            var x = targetPos.X - currentPos.X;
            var y = targetPos.Y - currentPos.Y;
            var z = targetPos.Z - currentPos.Z;

            return (x * x + y * y + z * z) * 100;
        }
    }
}
